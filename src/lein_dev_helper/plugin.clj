(ns lein-dev-helper.plugin
  (:require [leiningen.core.main :as lein]))

(defn middleware
  [project]
  (let [auto-refresh? (and (:root project)
                           (:auto-refresh project))
        deps (->> (get-in project [:profiles :dependencies])
                  (merge (get project :dependencies))
                  (map first)
                  (into #{}))
        included-ns-tool? (or (contains? deps 'tools.namespace)
                              (contains? deps 'org.clojure/tools.namespace))]
    (cond-> project
      (and (not included-ns-tool?)
           auto-refresh?)
      (update :dependencies concat [['org.clojure/tools.namespace "0.3.0-alpha4"]])

      auto-refresh?
      (update-in [:repl-options :init]
                 (fn [form]
                   (let [dirs (->> [:source-paths :test-paths]
                                   (select-keys project)
                                   (vals)
                                   (apply concat)
                                   (vec))
                         rf
                         `(binding [*ns* *ns*, *e nil]
                            ;; TODO:
                            ;;  - Remove the namespace of deleted file.
                            (apply clojure.tools.namespace.repl/set-refresh-dirs ~dirs)
                            (loop [first?# true
                                   tracker# (dissoc (apply clojure.tools.namespace.dir/scan
                                                           (clojure.tools.namespace.track/tracker)
                                                           ~dirs)
                                                    :clojure.tools.namespace.track/load
                                                    :clojure.tools.namespace.track/unload)]
                              (if first?#
                                (alter-var-root #'clojure.tools.namespace.repl/refresh-tracker
                                                (constantly tracker#))
                                (let [is-clj-file?# (partial re-find #"\.clj[cs]?$")
                                      repl-state#
                                      (reduce (fn [out# ns#]
                                                (let [ns-state# (some-> ns# (find-ns) (ns-interns))]
                                                  (assoc out# ns#
                                                         (some->> ns-state#
                                                                  (remove #(-> %
                                                                               (second)
                                                                               (meta)
                                                                               (:file "")
                                                                               (is-clj-file?#)))
                                                                  (map (juxt first #(-> % (second) (var-get))))
                                                                  (into {})))))
                                              {} (:clojure.tools.namespace.track/load tracker#))]
                                  (if-not (= :ok (clojure.tools.namespace.repl/refresh))
                                    (clojure.repl/pst *e))
                                  ;; Restores the var defined in REPL.
                                  (doseq [[ns# sym-val#] repl-state#]
                                    ;; If an error occurs during reloading,
                                    ;;  need to create namespace for restore the states.
                                    (create-ns ns#)
                                    (doseq [[sym# val#] sym-val#]
                                      (intern ns# sym# val#)))))
                              ;; Fire `on-reload` hook.
                              (if-let [sym# (some-> ~auto-refresh? :on-reload (symbol))]
                                (when-let [ns# (some-> (namespace sym#) (symbol))]
                                  (when-not first?#
                                    (try
                                      (require ns#)
                                      (catch Exception e#
                                        (println "An error is occurred on loading" (str "'" sym# "'") "function:")
                                        (clojure.repl/pst e#)))
                                    (try
                                      (some-> sym#
                                              (resolve)
                                              (deref)
                                              (.invoke tracker#))
                                      (catch Exception e#
                                        (println "An error is occurred on evaluating" (str "'" sym# "'") "function:")
                                        (clojure.repl/pst e#))))))
                              (recur nil
                                     (loop [new-tracker# (dissoc tracker# :clojure.tools.namespace.track/load)]
                                       (if-not (= (:clojure.tools.namespace.dir/time new-tracker#)
                                                  (:clojure.tools.namespace.dir/time tracker#))
                                         new-tracker#
                                         (do
                                           (Thread/sleep 1000)
                                           (recur (apply clojure.tools.namespace.dir/scan
                                                         tracker# ~dirs))))))))]
                     `(do
                        (require 'clojure.repl)
                        (require 'clojure.tools.namespace.dir)
                        (require 'clojure.tools.namespace.repl)
                        (require 'clojure.tools.namespace.track)
                        (let [file# (clojure.java.io/as-file ".auto-refresh")]
                          (when-not (.exists file#)
                            (.createNewFile file#)
                            (.addShutdownHook (Runtime/getRuntime) (Thread. #(.delete file#)))
                            (.start (Thread.
                                     (fn []
                                       (println "Start a refresher thread,"
                                                "watching dirs:"
                                                (->> ~dirs
                                                     (interpose ", ")
                                                     (apply str)))
                                       (loop []
                                         (try
                                           (Thread/sleep 1000)
                                           (eval ~rf)
                                           (catch Exception e#
                                             (println e#)))
                                         (recur)))))))
                        ~form)))))))
