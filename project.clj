(defproject lein-dev-helper "0.2.0"
  :description "Add a dependencies to develop"
  :url "https://gitlab.com/vmfhrmfoaj/lein-dev-helper"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :profiles {:dev {:main lein-dev-helper.plugin
                   :dependencies [[org.clojure/clojure "1.8.0"]
                                  [leiningen-core "2.7.1"]]}})
